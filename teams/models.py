from django.db import models
from django.conf import settings


class Department(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class TeamMember(models.Model):
    name = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    picture = models.URLField(null=True)
    description = models.TextField(null=True)
    is_department_head = models.BooleanField(default=False)
    username = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="team_members",
        on_delete=models.CASCADE,
    )
    department = models.ForeignKey(
        Department,
        related_name="team_members",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

from django.urls import path
from teams.views import department_list, show_department, search


urlpatterns = [
    path("", department_list, name="department_list"),
    path("<int:id>", show_department, name="show_department"),
    path("search", search, name="search"),
]

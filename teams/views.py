from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from teams.models import Department, TeamMember
from projects.models import Project


@login_required
def department_list(request):
    departments = Department.objects.all()
    context = {
        "department_list": departments,
    }
    return render(request, "teams/list.html", context)


@login_required
def show_department(request, id):
    department = get_object_or_404(Department, id=id)
    context = {
        "department_object": department,
    }
    return render(request, "teams/detail.html", context)


def show_team_members(request, id):
    team_members = TeamMember.objects.get(id=id)
    context = {
        "team_members": team_members,
    }
    return render(request, "teams/detail.html", context)


def search(request):
    if request.method == "POST":
        searched = request.POST.get("searched", False)
        departments = Department.objects.filter(name__contains=searched)
        projects = Project.objects.filter(name__contains=searched)
        team_members = TeamMember.objects.filter(name__contains=searched)
        context = {
            "searched": searched,
            "departments": departments,
            "projects": projects,
            "team_members": team_members,
        }
        return render(request, "teams/search.html", context)

    else:
        return render(request, "teams/search.html")
